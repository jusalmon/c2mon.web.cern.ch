# User Guide

The user guide assumes that you have already [installed C2MON](../installation) and have read the [overview](../overview)
to understand the basic concepts.